'use strict';

/* App Module */

angular.module('triggerType', ['angular-websocket'])
    .controller('RoomController', ['$scope', '$websocket', '$window', function($scope, $websocket, $window) {
        var rc = this;
        rc.sounds = {
            cylinderSpin: new Audio("/sounds/CylinderSpin.wav"),
            dryFire: new Audio("/sounds/DryFire.wav"),
            gunShot: new Audio("/sounds/GunShot.wav"),
            slide: new Audio("/sounds/Slide.wav")
        };
        rc.players = [{},{}];
        rc.player = {};
        rc.text = "";
        //open websocket to receive information
        //bind websocket handlers

        var socket = new SockJS("/stomp");
        var client = Stomp.over(socket);

        client.connect({}, function() {
            client.subscribe("/topic/data", function(message) {
                var msg = JSON.parse(message.body);
                console.log(message);
                switch(msg.opcode){
                    case 'pivot':
                        rc.players.reduce(function(it){
                            return it.id == msg.player.id
                        }).pivot = msg.player.pivot;
                        break;
                    case 'loginSuccess':
                        rc.player = msg.player;
                        rc.players = msg.players;
                        /*msg.players.forEach(function(it){
                           rc.players.push(it);
                        });*/
                        break;
                    case 'playerUpdate':
                        if(!msg.player.id == rc.player.id){
                            rc.players.reduce(function(it){
                                return it.id == msg.player.id
                            }).pivot = msg.pivot;
                        }
                        break;
                    case 'playSound':
                        rc.sounds[msg.soundName].play();
                        break;
                    case 'setText':
                        rc.text = msg.text;
                }
                console.log(message.body);
            });
            rc.send({opcode:'login'})
        });

        rc.send = function(packet){
            packet.player = rc.player;
            client.send("/app/data", {}, JSON.stringify(packet));
        };

        $scope.$watch(rc.player.pivot, function(oldval, newval){
            //when pivot changes this will notify the other players
            rc.send({opcode:'pivot'});
        })
    }])
    .directive('typer',
        [function() {

            function link(scope, element, attrs) {
                scope.pivot = 0;

                function updateHighlightedText() {
                    getTypedElement().innerHTML = scope.text.slice(0, scope.user.pivot);
                    getUntypedElement().innerHTML = scope.text.slice(scope.user.pivot, scope.text.length - 1);
                }

                function getTypedElement(){
                    return angular.element(element[0].querySelector('.typed'));
                }

                function getUntypedElement(){
                    return angular.element(element[0].querySelector('.untyped'));
                }

                element.bind("keydown keypress", function (event) {
                    var keyPressed = String.fromCharCode(event.keyCode ? event.keyCode : event.which);
                    if(keyPressed == scope.text[scope.user.pivot] ){
                        //good key press
                        scope.user.pivot++;
                    }
                    if(event.which === 13) { //enter
                        event.preventDefault();
                    }
                });

                scope.$watch(scope.text, function(newval, oldval) {
                    if(newval != oldval)
                    scope.user.pivot = 0;
                    //this way the pivot gets reset whenever a new paragraph comes in
                });
                scope.$watch(scope.user.pivot, function(){
                    //when scope.user.pivot changes this will fire
                    updateHighlightedText();
                })

            }

            return {
                scope: {
                    text: '=',
                    user: '=',
                    messageHandler: '@'
                },
                link: link
            };
        }]);
