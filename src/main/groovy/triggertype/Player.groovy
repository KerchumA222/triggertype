package triggertype

import grails.validation.Validateable

class Player implements Validateable {
    long id
    String name
    long pivot

    static constraints = {
        name(nullable: true)
    }
}
