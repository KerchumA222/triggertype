package triggertype

import grails.transaction.Transactional
import org.grails.web.json.JSONElement

@Transactional
class PlayerService {
    List<Player> playerList = []
    long lastId = -1

    def getOrCreatePlayer(JSONElement msg) {
        Player player
        if (msg.player?.id){
            player = getById(Long.parseLong(msg.player.id))
        }
        if (!player){
            player = findByName(msg?.player?.name)
        }
        if (!player){
            player = save(new Player())
        }
        player
    }
    def getAllPlayers(){
        playerList
    }
    def save(Player player){
        Player found = playerList.findResult {
            if(it.id == player.id)
                return it
        }
        if(found){
            found?.name = player.name
            found?.pivot = player.pivot
            return found
        } else {
            player.id = lastId++;
            playerList.push(player)
            return player
        }
    }
    def findByName(String name){
        def found = playerList.find {it.name == name}
        found
    }
    def getById(long Id){
        def found = playerList.find {if(it.id == id) return it}
        found
    }
}
