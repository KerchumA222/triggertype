package triggertype

import grails.converters.JSON
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessagingTemplate

class GameRoomController {
    ContentService contentService
    PlayerService playerService
    @Autowired
    SimpMessagingTemplate brokerMessagingTemplate

    def index() {
        def paragraph = contentService.getRandomParagraph()
        [data:paragraph]
    }
    def sendSound(String sound){
        sendMessage '{"opcode":"playSound", "soundName": "'+sound.toString()+'"}'
        respond([])
    }
    @MessageMapping("/data")
    @SendTo("/topic/data")
    protected String data(String packet) {
        def msg = JSON.parse packet
        Player player = playerService.getOrCreatePlayer(msg)

        switch(msg.opcode){
            case 'login':
                sendMessage ([
                    opcode: "loginSuccess",
                    player: player,
                    players: playerService.getAllPlayers(),
                    text: contentService.randomParagraph
                ])
                break;
            case 'playerUpdate':
                player.name = msg.player.name;
                playerService.save(player)
                sendMessage ([
                        opcode: "playerUpdate",
                        player: player
                ])
                break;
            case 'pivot':
                /*return {
                    opcode:'pivot'
                    pivot:msg.pivot
                    playerName:msg.name
                }*/
            case 'playSound':
                break;
            case 'setText':
                break;
        }
        return msg
    }
    protected sendMessage(Map message){
        sendMessage message as JSON
    }
    protected sendMessage(JSON message){
        brokerMessagingTemplate.convertAndSend '/topic/data', message.toString(true)
    }
    protected sendMessage(String message){
        brokerMessagingTemplate.convertAndSend '/topic/data', message
    }
}
