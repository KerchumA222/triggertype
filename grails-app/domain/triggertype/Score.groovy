package triggertype
class Score {
    String user
    Integer score

    static constraints = {
        user(maxSize: 1024)
        score(nullable: false)
    }
}
