package triggertype

import grails.io.IOUtils
import grails.transaction.Transactional

@Transactional
class ContentService {
    Random random = new Random()

    def getRandomNumber(int num){
        return random.nextInt(10 * num)
    }

    def getRandomParagraph(){
        def i = 0

        def paragraphs = getArrayOfParagraphs()
        def randomindex = getRandomNumber(paragraphs.size())%paragraphs.size()
        def returnParagraph = paragraphs[randomindex].toString()
        /*while(returnParagraph.size() <= 100){
            returnParagraph = returnParagraph+paragraphs[randomindex+i]
            i++
        }*/
        return returnParagraph
    }
    def getArrayOfParagraphs(){
        String bigString = getContent();
        def arrayofparagraphs = []
        int istart = 0
        int iend = 0
        for (character in bigString){
            if(character == '\n'){
                arrayofparagraphs.add( bigString[istart..iend]);
                istart = iend+ 1
                iend++
            }else{
                iend++
            }
        }
        /*def count = arrayofparagraphs.size()
        for(element in arrayofparagraphs){ // this messes stuff up don't know why
            if(element == '\n'){
                arrayofparagraphs.remove(element)
            }
        }*/
        return arrayofparagraphs
    }
    def getContent() {
        InputStream textFile = this.class.classLoader.getResourceAsStream('text/fear_and_loathing.txt');
        String bigString = IOUtils.toString(textFile, "UTF-8");
        bigString
    }
}
