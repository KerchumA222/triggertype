import triggertype.ContentService
import triggertype.PlayerService

// Place your Spring DSL code here
beans = {
    contentService(ContentService)
    playerService(PlayerService)
}
