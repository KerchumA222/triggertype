/**
 * Created by Chris on 10/10/2015.
 */
var a = document.getElementById("text_to_be_typed");
a.style.visibility = "hidden";
var textToType = document.getElementById("text_to_be_typed").innerHTML;

var typedchars = document.getElementById("player1_typed");
var untypedchars = document.getElementById("player1_untyped");
untypedchars.innerHTML =  textToType;
var pivot = 0;
var key;

document.onkeypress = function (e) {
    e = e || window.event;
    key = String.fromCharCode(e.keyCode ? e.keyCode : e.which);
    if(key == textToType[pivot]){
        console.log(key);
        pivot ++;
        typedchars.innerHTML = textToType.slice(0, pivot);
        untypedchars.innerHTML = textToType.slice(pivot, textToType.length -1)

    }
};

