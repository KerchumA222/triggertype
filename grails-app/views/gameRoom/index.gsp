<%@ page import="javax.servlet.ServletRequest" %>
<!DOCTYPE html>
<html>
<head>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.7/angular.min.js"></script>
    <asset:javascript src="angular-websocket.min.js"/>
    <asset:javascript src="spring-websocket"/>
    <asset:stylesheet src="versusStyle" />
    <script type="text/javascript">
        window.contextPath = location.hostname+(location.port ? ':'+location.port: '');
    </script>
    <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <asset:javascript src="app.js"/>
    <title>TriggerType</title>
</head>
<body data-ng-app="triggerType" data-ng-controller="RoomController">
<div class="wrapper">
    <div class="pure-g">
        <div class="pure-u-5-5 banner">
            <asset:image src="TriggerType.png" alt="Type or DIE!"/>
        </div>
    </div>

    <div class="pure-g" id="middle-content">
        <div class="pure-u-6-24" id="left-player">
            <h2>{{rc.players[1].name}}</h2>
            <typer data-user="false" data-text="rc.text" data-pivot="rc.players[1].pivot"/>
        </div>
        <div class="pure-u-10-24" id="main-graphics">
            <div id="img-content">
                <asset:image src="Revolver_Right.png" alt="Type or DIE!"/>
            </div>
        </div>
        <div class="pure-u-6-24" id="right-player">
            <h2>Right Player</h2>
        </div>
    </div>

    <div class="page-body" id="main-player" role="main">
        <h1><input type="text" data-ng-model="rc.player.name" width="315"></h1>
        <typer data-text="rc.text" data-user="rc.player"/>

        <span id="player1_typed" class="typed"></span>
        <span id="player1_untyped" class="untyped"></span>

        <p id="text_to_be_typed">${data}</p>
    </div>
</div>
<asset:javascript src="textCursor" />
</body>
</html>
